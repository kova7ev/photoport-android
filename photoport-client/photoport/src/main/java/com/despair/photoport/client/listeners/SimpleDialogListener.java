package com.despair.photoport.client.listeners;

/**
 * Created by root on 6/12/14.
 */
public interface SimpleDialogListener {
    public void OnPositiveButtonClick();
    public void OnNegativeButtonClick();
}
